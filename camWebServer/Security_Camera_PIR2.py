from picamera import PiCamera
from time import sleep
import smtplib
gmail_user = 'example@domain.com'
gmail_password = 'Password!'
import time
from datetime import datetime
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
import RPi.GPIO as GPIO
import time

toaddr = 'example2@domain.com'
me = 'example@domain.com'
Subject = 'Notificación de Seguridad'

GPIO.setmode(GPIO.BCM)

P=PiCamera()
P.resolution= (1024,768)
P.start_preview()

try:
    print("Motion...")
    #camera warm-up time
    P.capture('Reporte.jpg')
    time.sleep(4)
    subject='Notificación de seguridad: ¡Se ha activado el sistema!'
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = me
    msg['To'] = toaddr

    fp= open('Reporte.jpg','rb')
    img = MIMEImage(fp.read())
    fp.close()
    msg.attach(img)

    server = smtplib.SMTP_SSL('smtp.gmail.com',465)
    server.login(gmail_user, gmail_password)
    server.send_message(msg)
    server.quit()
finally:
    P.close()
